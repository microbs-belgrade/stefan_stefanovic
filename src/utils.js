export const injectAlbum = (listItems, albums, setListItems) => {
  const sortedData = albums.sort((a, b) =>
    b.collectionName.slice().localeCompare(a.collectionName.slice())
  );
  const newAlbums = [...listItems];

  setInterval(() => {
    sortedData.forEach((album) => {
      if (!newAlbums.includes(album)) {
        newAlbums.splice(listItems[listItems.length - 1], 1, album);
      }
    });
  }, 1000);
  setListItems(newAlbums);
};

export const getAlbums = async (albumToSearch) => {
  try {
    const link = `https://itunes.apple.com/search?term=${albumToSearch}&limit=5&entity=song`;
    const res = await fetch(link, {
      method: "GET",
    });
    return await res.json();
  } catch (err) {
    console.log(err);
  }
};

export const albumRotate = (albumArray) => {
  albumArray.push(albumArray.shift());
  return albumArray;
};
