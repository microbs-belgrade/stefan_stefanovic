const AlbumList = ({ listItems }) => (
  <div className="list">
    <ul>
      {listItems.map(
        (
          {
            trackId,
            collectionName,
            collectionViewUrl,
            artworkUrl100,
            artistName,
            trackName,
            char,
          },
          index
        ) => (
          <li
            key={char || `${trackId}-${index}`}
            className={char ? "char" : "album"}
          >
            {char ? (
              <p>{char}</p>
            ) : (
              <a href={collectionViewUrl} target="_blank">
                <img src={artworkUrl100} alt="album-cover" />
                <div>
                  <p className="album-name">
                    <span>Album name: </span>
                    {collectionName}
                  </p>
                  <p className="song-name">
                    <span>Song name: </span>
                    {trackName}
                  </p>
                  <p className="album-artist">
                    <span>Artist: </span>
                    {artistName}
                  </p>
                </div>
              </a>
            )}
          </li>
        )
      )}
    </ul>
  </div>
);

export default AlbumList;
