const AlbumInput = ({ onAlbumSearch, albumToSearch }) => (
  <div className="search">
    <input
      type="text"
      placeholder="Search for an album"
      onChange={({ target }) => onAlbumSearch(target.value)}
      value={albumToSearch}
    />
  </div>
);

export default AlbumInput;
