import { useEffect, useState } from "react";

import { albumRotate, getAlbums, injectAlbum } from "utils";
import AlbumInput from "components/album/album-input";
import AlbumList from "components/album/album-list";

import "styles/app.css";

const initialState = [
  { char: "A" },
  { char: "B" },
  { char: "C" },
  { char: "D" },
  { char: "E" },
];

const App = () => {
  const [albumToSearch, setAlbumToSearch] = useState("");
  const [listItems, setListItems] = useState(initialState);
  const [albums, setAlbums] = useState([]);

  const handleOnAlbumChange = (album) => {
    setAlbumToSearch(album);
  };

  const searchAlbums = async () => {
    const data = await getAlbums(albumToSearch);
    setAlbums(data.results);
  };

  useEffect(() => {
    if (albumToSearch) searchAlbums();
  }, [albumToSearch]);

  useEffect(() => {
    const rotateAfterOneSecond = setInterval(() => {
      const rotatedAlbums = albumRotate(listItems);
      setListItems(rotatedAlbums);
      injectAlbum(listItems, albums, setListItems);
    }, 1000);
    return () => clearInterval(rotateAfterOneSecond);
  }, [albums, listItems]);

  return (
    <div className="app">
      <AlbumInput
        onAlbumSearch={handleOnAlbumChange}
        albumToSearch={albumToSearch}
      />
      <AlbumList listItems={listItems} />
    </div>
  );
};

export default App;
